<h1>Riot ES6 Template</h1>
<p>Cool template to build riot apps in ES6!</p>

<h2>Setup</h2>
<p>
    Clone the repo and run <code>yarn</code><br />
    If you need yarn run this command: <code> npm install -g yarn </code>
</p>

<h2>Example</h2>

<h4>

</h4>

<h4>app.js</h4>

<p>
app.js is our entry point for the app.
Here we import all the libraries we need, the tags and the store.
At last we mount all the tags.
</p>

```javascript
import riot from 'riot'
import 'riot-hot-reload'
import './app-title.tag'
import './change-title.tag'
import './store'

riot.mount('*');
```
<h4>store.js</h4>

<p>
    The store.js file is where we define our app store.
    Because it extends the riot.observable class, we inherit all the relative methods.
    Here we extend it with variables and methods that we need,
    and then, we create an instance of the Store class and we pass it to the global mixin,
    so we can use it in all the tags.
    In this case we have our title variable, and the setTitle method, that when will be
    called from the tag, will set the title with the title passed, and will trigger the
    'CHANGE_TITLE' custom event, that will update the tag listening to it, to display
    the new value.
</p>

```javascript
import riot from 'riot';

class Store extends riot.observable {
    
    title = 'Cool App'

    setTitle = (title) => {
        this.title = title
        this.trigger('CHANGE_TITLE')
    }
}

riot.mixin({ store: new Store() });
```
<p>
    Because we are using the babel plugin <a href="https://babeljs.io/docs/plugins/transform-class-properties/">transform-class-properties</a>,
    we don't need to define the constructor.
</p>

<h4>app-title.tag</h4>

<p>
    This is our first tag.
    Here we listen to the custom event 'CHANGE_TITLE' 
    and then we update the component to display the new title.
</p>

```html
<app-title>

    <h3>{store.title}</h3>

    <script>

        this.store.on('CHANGE_TITLE', () => this.update() )
        
    </script> 

</app-title>
```
<p>

<h4>change-title.tag</h4>

<p>
    In this tag we have the input to update our app title.
    When we unfocus the input we call the changeTitle function,
    we get the value from the input, and we call the setTitle store method.
</p>

```html
<change-title>

    <input type="text" ref="title" onchange={changeTitle} value={store.title}>

    <script>

        this.changeTitle = () => {
            let title = this.refs.title.value
            this.store.setTitle(title)
            this.refs.title.blur()
        }
        
    </script>

</change-title>
```

</p>

<p>
That's it! Enjoy!
</p>