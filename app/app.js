import riot from 'riot'
import 'riot-hot-reload'
import './app-title.tag'
import './change-title.tag'
import './store'

riot.mount('*');
