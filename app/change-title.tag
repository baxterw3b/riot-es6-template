<change-title>

    <input type="text" ref="title" onchange={changeTitle} value={store.title}>

    <script>

        this.changeTitle = () => {
            let title = this.refs.title.value
            this.store.setTitle(title)
            this.refs.title.blur()
        }
        
    </script>

</change-title>