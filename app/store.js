import riot from 'riot';

class Store extends riot.observable {
    
    title = 'Cool App'

    setTitle = (title) => {
        this.title = title
        this.trigger('CHANGE_TITLE')
    }
}

riot.mixin({ store: new Store() });